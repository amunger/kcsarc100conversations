using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OneHundredConversations.Controls;
using OneHundredConversations.iOS;
using UIKit;
using OneHundredConversations.Enums;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(IconTabbedPage), typeof(IconTabbedPageRenderer))]
namespace OneHundredConversations.iOS
{
    class IconTabbedPageRenderer : TabbedRenderer
    {
        private Dictionary<string, string> iconUrls = new Dictionary<string, string>()
        {
            {PageUtils.ToString(PageUtils.PageName.Topics), "Chat-50.png"},
            {PageUtils.ToString(PageUtils.PageName.AboutUs), "Info-50.png"},
            {PageUtils.ToString(PageUtils.PageName.Search), "Search-50.png"}
        };

        public override void ViewWillAppear(bool animated)
        {
            base.ViewWillAppear(animated);
            var items = TabBar.Items;

            for (var i = 0; i < items.Length; i++)
            {
                var _myImage = UIImage.FromBundle(iconUrls[items[i].Title]);
                var _mySelectedImage = UIImage.FromBundle(iconUrls[items[i].Title]); 
                items[i].SetFinishedImages(_mySelectedImage, _myImage);
            }
        }
    }
}
