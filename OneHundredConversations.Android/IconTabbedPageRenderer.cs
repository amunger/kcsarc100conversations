﻿using OneHundredConversations;
using OneHundredConversations.Controls;
using Java.Util;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(IconTabbedPage), typeof(IconTabbedPageRenderer))]

namespace OneHundredConversations
{
    public class IconTabbedPageRenderer : TabbedRenderer
    {
    }
}