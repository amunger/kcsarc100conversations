﻿using System;
using System.IO;
using Android.App;
using Android.Content.PM;
using Android.OS;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Environment = System.Environment;

namespace OneHundredConversations
{
    [Activity (Label = "100Conversations", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class Activity1 : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
    {
        public static Activity1 ShareActivityContext;

        protected override void OnCreate (Bundle bundle)
        {
            base.OnCreate (bundle);

            global::Xamarin.Forms.Forms.Init(this, bundle);

            LoadApplication(new App()); // method is new in 1.3
        }

    }
}


