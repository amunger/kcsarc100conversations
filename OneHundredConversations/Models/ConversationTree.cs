﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneHundredConversations
{
    public class ConversationTree
    {
        public ConversationTree(){}
        public IEnumerable<ConversationTopic> Topics { get; set; }
    }

    public class ConversationTopic
    {
        public string Title { get; set; }
        public string FormattedTitle { get { return Title; } }

        public int ConversationCount
        {
            get { return Conversations != null ? Conversations.ToList().Count : 0; }
        }
        public IEnumerable<Conversation> Conversations { get; set; }
    }

    public class Conversation 
    {
        public string Title { get; set; }
        public int Id { get; set; }

        public IEnumerable<Section> Sections { get; set; }
    }

    public class Section
    {
        public string Title { get; set; }
        public string Body { get; set; }
        public string TitleBody { get { return Title + ":\n" + Body; } }

        public string LineBrokenBody
        {
            get { return Body.Replace("\n", "\n\n"); }
        }
    }
}
