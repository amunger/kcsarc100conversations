﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OneHundredConversations
{
    class AboutUsClass
    {
        public IEnumerable<AboutUsTopic> Topics { get; set; }
    }

    public class AboutUsTopic
    {
        public string Title { get; set; }
        public string Body { get; set; }
    }
}
