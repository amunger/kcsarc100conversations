﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OneHundredConversations.Enums
{
    public class PageUtils
    {
        public enum PageName
        {
            Topics = 1,
            AboutUs = 2,
            Search = 3
        };

        public static string ToString(PageName name)
        {
            switch (name)
            {
                case PageName.Topics:
                    return "Topics";
                case PageName.AboutUs:
                    return "About";
                case PageName.Search:
                    return "Search";
                default:
                    return "???";
            }
        }
    }
    
}
