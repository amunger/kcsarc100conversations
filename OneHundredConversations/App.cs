﻿using System;
using System.Collections.Generic;
using OneHundredConversations.Controls;
using OneHundredConversations.Enums;
using OneHundredConversations;
using Xamarin.Forms;
using OneHundredConversations.Data;
using OneHundredConversations.Views;

namespace OneHundredConversations
{
    public class App : Application
    {

        public App()
        {
            MainPage = GetMainPage();
        }
        private static Page GetMainPage ()
        {
            ConversationManager cm = new ConversationManager();

            var page = new IconTabbedPage();             
            page.Children.Add(GetAbout());
            page.Children.Add(GetTopics(cm));
            page.Children.Add(GetSearchPage(cm));
            page.BackgroundColor = Color.White;
            return page;
        }

        private static NavigationPage GetSearchPage(ConversationManager cm)
        {
            return new NavigationPage(new SearchPage(cm))
            {
                Title = PageUtils.ToString(PageUtils.PageName.Search),
                BarBackgroundColor = HeaderTint,
                BarTextColor = NavTint
            };
        }

        private static NavigationPage GetTopics(ConversationManager cm)
        {
            return new NavigationPage(new TopicsPage(cm))
            {
                Title = PageUtils.ToString(PageUtils.PageName.Topics),
                BarBackgroundColor = HeaderTint,
                BarTextColor = NavTint 
            };
        }

        private static NavigationPage GetAbout()
        {
            return new NavigationPage(new AboutPage())
            {
                Title = PageUtils.ToString(PageUtils.PageName.AboutUs),
                BarBackgroundColor = HeaderTint,
                BarTextColor = NavTint
            };
        }

        public static string Name
        {
            get { return "100 Conversations"; }
        }
        public static Color NavTint {
            get { return Color.White; }
        }
        public static Color HeaderTint {
            get { return Color.FromHex("#333333"); }
        }
        public static Color BackgroundTint {
            get { return Color.White; }
        }
    }
}

