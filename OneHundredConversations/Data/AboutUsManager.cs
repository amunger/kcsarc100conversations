﻿namespace OneHundredConversations.Data
{
    class AboutUsManager
    {
        private AboutUsClass aboutUsDesc;
        private AboutUsLoader loader;

        public AboutUsManager()
        {
            loader = new AboutUsLoader();
            aboutUsDesc = loader.Load();
        }

        public AboutUsClass TopicTree()
        {
            return aboutUsDesc;
        }
    }
}
