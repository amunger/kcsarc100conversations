﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OneHundredConversations
{
    public class DummyData
    {
        public static ConversationTree Tree = new ConversationTree
        {
            Topics = new List<ConversationTopic>
            {
                new ConversationTopic
                {
                    Title = "Boundaries & Values",
                    Conversations = new List<Conversation>
                    {
                        new Conversation
                        {
                            Title = "Your Body",
                            Sections = new List<Section>
                            {
                                new Section
                                {
                                    Title = "This Conversation Will Help You…",
                                    Body = "1. Name one positive aspect about their body" +
                                           "2. Say ways in which their body helps them in life" +
                                           "3. Say ways that they might view their own body differently than others may view their own bodies" 
                                },
                                new Section
                                {
                                    Title = "Think About This First",
                                    Body = "This conversation is best had with a close group of people" +
                                           "This conversation has two activities that will help folks think about this topic"
                                }
                            }
                        },
                        new Conversation
                        {
                            Title = "Your Booty",
                            Sections = new List<Section>
                            {
                                new Section
                                {
                                    Title = "This Conversation Will Help You…",
                                    Body = "1. Name one positive aspect about their body" +
                                           "2. Say ways in which their body helps them in life" +
                                           "3. Say ways that they might view their own body differently than others may view their own bodies" 
                                },
                                new Section
                                {
                                    Title = "Think About This First",
                                    Body = "This conversation is best had with a close group of people" +
                                           "This conversation has two activities that will help folks think about this topic"
                                }
                            }
                        }
                    }
                }, 
                new ConversationTopic
                {
                    Title = "Friends & Family",
                    Conversations = new List<Conversation>
                    {
                        
                    }
                },
                new ConversationTopic
                {
                    Title = "Relationships",
                    Conversations = new List<Conversation>
                    {
                        
                    }
                }
            }
        };

    }
}
