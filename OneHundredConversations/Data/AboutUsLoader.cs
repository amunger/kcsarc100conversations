﻿using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace OneHundredConversations.Data
{
    class AboutUsLoader
    {
        private const string JsonName = "OneHundredConversations.aboutUs.json";
        public AboutUsClass Load()
        {
            string json = load_internal();
            AboutUsClass aboutUsContent = JsonConvert.DeserializeObject<AboutUsClass>(json);

            return aboutUsContent;
        }

        private static string load_internal()
        {
            var assembly = typeof(AboutUsLoader).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(JsonName);
            string text = "";
            using (var reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }
    }
}
