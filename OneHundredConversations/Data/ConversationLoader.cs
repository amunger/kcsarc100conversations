﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;

namespace OneHundredConversations.Data
{
    class ConversationLoader
    {
        private const string ConversationJsonName = "OneHundredConversations.conversations.json";
        public ConversationTree Load()
        {
            string json = load_internal();

            ConversationTree tree = new ConversationTree
            {
                Topics = JsonConvert.DeserializeObject<List<ConversationTopic>>(json)
            };
            return tree;
        }

        private static string load_internal()
        {
            var assembly = typeof(ConversationLoader).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream(ConversationJsonName);
            string text = "";
            using (var reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }

    }
}
