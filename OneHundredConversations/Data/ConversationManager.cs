﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace OneHundredConversations.Data
{
    public class ConversationManager
    {
        private ConversationTree conversations;
        private ConversationLoader loader;

        public ConversationManager()
        {
            loader = new ConversationLoader();
            conversations = loader.Load();
        }

        public ConversationTree TopicTree()
        {
            return conversations;
        }

        public List<Conversation> findConversationsByKeywords(string keywords)
        {
            keywords = new Regex("[^a-zA-Z ]").Replace(keywords, "").ToLower();
            List<string> tokenList = new List<String>(keywords.Split(null)); // splits on whitespace

            List<ScoredSearchResult> results = new List<ScoredSearchResult>();
            if (conversations.Topics != null)
            {
                foreach (ConversationTopic topic in conversations.Topics)
                {
                    if (topic.Conversations != null)
                    {
                        foreach (Conversation conv in topic.Conversations)
                        {
                            int currentScore = 0;
                            foreach (string token in tokenList)
                            {
                                currentScore += Regex.Matches(conv.Title, token, RegexOptions.IgnoreCase).Count * 10;
                                foreach (Section s in conv.Sections)
                                {
                                    currentScore += Regex.Matches(s.Body, token, RegexOptions.IgnoreCase).Count;
                                }
                            }
                            if (currentScore > 0)
                                results.Add(new ScoredSearchResult { TheConversation = conv, Score = currentScore });
                        }
                    }
                }
            }
            results.Sort();
            results.Reverse();
            List<Conversation> resultsToReturn = new List<Conversation>();
            foreach (ScoredSearchResult result in results)
            {
                resultsToReturn.Add(result.TheConversation);
            }
            return resultsToReturn;
        }

        public Conversation getById(int id)
        {
            return null;
        }

        private class ScoredSearchResult : IComparable<ScoredSearchResult>
        {
            public Conversation TheConversation { get; set; }
            public int Score { get; set; }
            public int CompareTo(ScoredSearchResult other)
            {
                if (other == null)
                    return 1;
                return this.Score.CompareTo(other.Score);
            }
        }
    }
}
;