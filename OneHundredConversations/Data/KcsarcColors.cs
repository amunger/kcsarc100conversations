﻿using Xamarin.Forms;

namespace OneHundredConversations.Data
{
    // Xamarin colors are double values between 0 and 1 for R, G, B, A channels
    public static class KcsarcColors
    {
        public static Color PrimaryYellow = new Color(255f / 256, 216f / 256, 56f / 256);
        public static Color PrimaryOrange = new Color(232f / 256, 109f / 256, 31f / 256);
        public static Color PrimaryGrey = new Color(96f / 256, 96f / 256, 91f / 256);

        public static Color AccentRed = new Color(181f / 256, 9f / 256, 56f / 256);
        public static Color AccentGreen = new Color(184f / 256, 179f / 256, 8f / 256);
        public static Color AccentBlue = new Color(0f / 256, 157f / 256, 220f / 256);
    };
}
