﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OneHundredConversations;
using OneHundredConversations.Data;
using Xamarin.Forms;

namespace OneHundredConversations
{
    public class TopicsPage : ContentPage
    {
        ListView listView;
        private ConversationManager cm;
        public TopicsPage (ConversationManager incomingCm)
        {
            cm = incomingCm;
            Title = App.Name;
            BackgroundColor = App.BackgroundTint;

            NavigationPage.SetHasNavigationBar (this, true);

            var title = new Label
            {
                Text = "List of Topics",
                TextColor = KcsarcColors.AccentBlue,
                FontSize = 20,
                FontAttributes = FontAttributes.Bold
            };

            listView = new ListView {
                RowHeight = 60,
                Footer = ""
            };
            // see the SessionCell implementation for how the variable row height is calculated
            listView.HasUnevenRows = true;

            listView.ItemsSource = cm.TopicTree().Topics;
            listView.ItemTemplate = new DataTemplate(() =>
            {
                Label name = new Label
                {
                    TextColor = KcsarcColors.PrimaryGrey,
                    FontSize = 20,
                    FontAttributes = FontAttributes.Bold
                };
                name.SetBinding(Label.TextProperty, "FormattedTitle");

                return new ViewCell
                {
                    View = new StackLayout
                    {
                        Padding = new Thickness(5),
                        Children =
                        {
                            name
                        }
                    }
                };
            });
            listView.ItemTapped += (sender, e) => {
                var conversation = e.Item as ConversationTopic;
                var sessionPage = new ConversationPage {BindingContext = conversation};
                Navigation.PushAsync(sessionPage);
            };

            listView.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };

            Content = new StackLayout {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Padding = new Thickness(20),
                Children = {
                    title,
                    listView
                }
            };
        }
    }
}