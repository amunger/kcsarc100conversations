﻿using OneHundredConversations;
using OneHundredConversations.Controls;
using OneHundredConversations.Data;
using Xamarin.Forms;

namespace OneHundredConversations.Views
{
    class ConversationSectionsPage : ContentPage
    {
        private ListView listView;
        public Conversation conversation;
        public ConversationSectionsPage()
        {
            Title = App.Name;
            BackgroundColor = App.BackgroundTint;

            NavigationPage.SetHasNavigationBar(this, true);
            ContentView title = GenerateConversationLabel();
            GenerateConversationListView();

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children =
                {
                    title,
                    listView
                }
            };
        }

        private void GenerateConversationListView()
        {
            listView = new ListView
            {
                HasUnevenRows = true,
                Footer = ""
            };
            listView.SetBinding(ListView.ItemsSourceProperty, "Sections");
            listView.ItemTemplate = new DataTemplate(() =>
            {
                var cellTitle = new Label
                {
                    TextColor = KcsarcColors.PrimaryGrey,
                    FontSize = 18,
                    FontAttributes = FontAttributes.Bold
                };
                cellTitle.SetBinding(Label.TextProperty, "Title");
                var cellBody = new Label
                {
                    Text = "Conversation Title",
                    TextColor = KcsarcColors.PrimaryGrey,
                    FontSize = 16
                };
                cellBody.SetBinding(Label.TextProperty, "LineBrokenBody");
                return new WrappedTextCell(cellTitle, cellBody);
            }
                );
            listView.ItemSelected += (sender, e) => { ((ListView) sender).SelectedItem = null; };
        }

        private static ContentView GenerateConversationLabel()
        {
            var label = new Label
            {
                Text = "Conversation Title",
                TextColor = KcsarcColors.AccentBlue,
                FontSize = 18,
                FontAttributes = FontAttributes.Bold
            };
            label.SetBinding(Label.TextProperty, "Title");
            var title = new ContentView
            {
                Padding = new Thickness(20, 20, 5, 5),
                Content = label
            };
            return title;
        }
    }
}
