﻿using System.Linq;
using OneHundredConversations.Views;
using OneHundredConversations.Data;
using Xamarin.Forms;

namespace OneHundredConversations
{
    public class ConversationPage : ContentPage
    {
        ListView listView;
        public ConversationPage ()
        {
            Title = App.Name;
            BackgroundColor = App.BackgroundTint;

            NavigationPage.SetHasNavigationBar (this, true);

            var title = new Label { 
                Text = "Conversation Title",
                TextColor = KcsarcColors.AccentBlue,
                FontSize = 18,
                FontAttributes = FontAttributes.Bold
            };
            title.SetBinding (Label.TextProperty, "Title");

            listView = new ListView
            {
                RowHeight = 40,
                Footer = ""
            };
            // see the SessionCell implementation for how the variable row height is calculated
            listView.HasUnevenRows = true;
            listView.SetBinding(ListView.ItemsSourceProperty, "Conversations");
            listView.ItemTemplate = new DataTemplate(typeof(TextCell));
            listView.ItemTemplate.SetBinding(TextCell.TextProperty, "Title");
            listView.ItemTemplate.SetValue(TextCell.TextColorProperty, KcsarcColors.PrimaryGrey);

            listView.ItemTapped += (sender, e) =>
            {
                Conversation conversation = e.Item as Conversation;
                var sessionPage = new ConversationSectionsPage { BindingContext = conversation, conversation = conversation };
                Navigation.PushAsync(sessionPage);
            };

            listView.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.StartAndExpand,
                Padding = new Thickness(20),
                    Children = {
                        title, 
                        listView
                    }
            };
        }
    }
}