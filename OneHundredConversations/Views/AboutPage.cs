﻿using OneHundredConversations.Controls;
using OneHundredConversations.Data;
using Xamarin.Forms;
using System;

namespace OneHundredConversations.Views
{
    public class AboutPage : ContentPage
    {
        ListView listView;
        public AboutPage()
        {
            Title = App.Name;
            BackgroundColor = Color.White;
            listView = new ListView
            {
                Footer = "",
                HasUnevenRows = true
            };
            AboutUsManager cm = new AboutUsManager();

            listView.ItemsSource = cm.TopicTree().Topics;
            listView.ItemTemplate = new DataTemplate(() =>
            {
                var cellTitle = new Label
                {
                    TextColor = KcsarcColors.PrimaryGrey,
                    FontSize = 18,
                    FontAttributes = FontAttributes.Bold
                };
                cellTitle.SetBinding(Label.TextProperty, "Title");
                var cellBody = new Label
                {
                    Text = "Conversation Title",
                    TextColor = KcsarcColors.PrimaryGrey,
                    FontSize = 16
                };
                cellBody.SetBinding(Label.TextProperty, "Body");
                return new WrappedTextCell(cellTitle, cellBody);
            });

            AddContactUsHandler();

            listView.SelectedItem = null;
            listView.ItemSelected += (sender, e) =>
            {
                ((ListView)sender).SelectedItem = null;
            };

            Content = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = { listView }
            };
        }

        private void AddContactUsHandler()
        {
            string emailAddress = "development@kcsarc.org";
            listView.ItemTapped += (sender, e) =>
            {
                var topic = e.Item as AboutUsTopic;
                if (topic.Title.Equals("-- Click Here to Contact Us --"))
                    Device.OpenUri(new Uri("mailto:" + emailAddress));
            };
        }
    }
}
