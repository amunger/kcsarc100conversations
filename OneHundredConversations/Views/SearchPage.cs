﻿using System.Collections.Generic;
using OneHundredConversations.Views;
using OneHundredConversations.Data;
using Xamarin.Forms;

namespace OneHundredConversations
{
    public class SearchPage : ContentPage
    {
        private ListView listView;
        private ConversationManager cm;

        public SearchPage (ConversationManager incomingCm)
        {
            cm = incomingCm;
            Title = App.Name;
            BackgroundColor = App.BackgroundTint;

            NavigationPage.SetHasNavigationBar (this, true);

            //var title = BuildSearchLabel();
            BuildListView();
            SearchBar searchBar = BuildSearchBar();
            var searchLayout = new StackLayout
            {
                VerticalOptions = LayoutOptions.FillAndExpand,
                Children = {
                    searchBar,
                    listView
                }
            };
            Content = searchLayout;
        }

        private static ContentView BuildSearchLabel()
        {
            var label = new Label
            {
                Text = "Search",
                TextColor = KcsarcColors.AccentBlue,
                FontSize = 20,
                FontAttributes = FontAttributes.Bold
            };
            label.SetBinding(Label.TextProperty, "Title");
            var title = new ContentView
            {
                Padding = new Thickness(20, 20, 5, 5),
                Content = label
            };
            return title;
        }

        private void BuildListView()
        {
            listView = new ListView
            {
                RowHeight = 40,
                Footer = ""
            };

            // see the SessionCell implementation for how the variable row height is calculated
            listView.HasUnevenRows = true;
            listView.SetBinding(ListView.ItemsSourceProperty, "Conversations");
            listView.ItemTemplate = new DataTemplate(typeof (TextCell));
            listView.ItemTemplate.SetBinding(TextCell.TextProperty, "Title");
            listView.ItemTemplate.SetValue(TextCell.TextColorProperty, KcsarcColors.PrimaryGrey);

            listView.ItemTapped += (sender, e) =>
            {
                Conversation conversation = e.Item as Conversation;
                var sessionPage = new ConversationSectionsPage {BindingContext = conversation, conversation = conversation};
                Navigation.PushAsync(sessionPage);
            };

            listView.ItemSelected += (sender, e) => { ((ListView) sender).SelectedItem = null; };
        }

        private SearchBar BuildSearchBar()
        {
            var searchBar = new SearchBar
            {
                BackgroundColor = KcsarcColors.AccentBlue
            };

            searchBar.SearchButtonPressed += (sender, e) =>
            {
                List<Conversation> conversations = cm.findConversationsByKeywords((sender as SearchBar).Text);
                listView.ItemsSource = conversations;
            };
            return searchBar;
        }
    }
}