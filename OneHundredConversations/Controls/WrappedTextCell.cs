﻿using Xamarin.Forms;

namespace OneHundredConversations.Controls
{
    public class WrappedTextCell : ViewCell
    {
        Label title, label;
        StackLayout layout;
        public WrappedTextCell(Label title, Label body): base()
        {
            this.title = title;
            this.label = body;

            layout = new StackLayout
            {
                Padding = new Thickness(20),
                Orientation = StackOrientation.Vertical,
                Children = { this.title, this.label }
            };

            View = layout;
        }
    }
}