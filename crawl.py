import bs4
from bs4 import BeautifulSoup
from enum import Enum
import re
import sys
import requests
import json
import natsort

# Constants
number_of_pages = 10
number_of_conversations_in_topic = 10

number_of_topics = number_of_pages
number_of_conversations = number_of_topics * number_of_conversations_in_topic

domain = "http://onehundredconversations.squarespace.com"
baseurl = domain + "/100conversations/?currentPage="
topicsurl = domain + "/topics/"
name = "onehundredconversations-spider"
start_urls = [baseurl + str(x) for x in range(1, number_of_pages + 1)]


# Classes
class States(Enum):
    HEADER = 1
    BODY = 2


class Section():
    def __init__(self):
        self.Title = ""
        self.Body = ""


class Conversation():
    def __init__(self):
        self.Id = 0
        self.Title = ""
        self.Sections = []


class Topic():
    def __init__(self):
        self.Title = ""
        self.Conversations = []


# Methods
def dumper(obj):
    try:
        return obj.toJSON()
    except:
        return obj.__dict__


def get_topic_titles():
    soup = BeautifulSoup(get_html_text(topicsurl))
    topics = []
    for topic in soup.findAll("h3"):
        if topic.span and topic.span.a:
            topics.append(topic.span.a.contents[0])

    return topics


def get_html_text(url):
    return requests.get(url).text


def get_urls():
    print("Finding urls of pages to transform:")
    all_urls = []
    for url in start_urls:
        html = get_html_text(url)
        matches = re.findall(
            "class=\"journal-entry-navigation-current\" ?href=\"([^\"]*)\"",
            html)
        all_urls.extend(matches)
        sys.stdout.write(".")

    print("\nFormatting URLs properly...")
    full_urls = []
    for url in all_urls:
        full_urls.append(domain + url)

    print ("\nFound this many pages to transform: " + str(len(full_urls)))

    return naturally_sort(full_urls)


def naturally_sort(lst):
    """Naturally sort the list, so the number 2 comes after 1, not 19"""
    return natsort.natsorted(lst, key=lambda y: y.lower())


def get_title(soup):
    """Extract the title of the conversation from the html"""
    candidates = soup.findAll("h2", {"class": "title"})[0]
    for candidate in candidates:
        if candidate.name == "a":
            return candidate.contents[0]


def add_newline_to_section(section):
    if section.Body:
        section.Body += '\n'


def get_sections(body):
    state = States.HEADER
    section = Section()
    sections = []
    for content in body.contents:
        if type(content) == bs4.element.NavigableString and len(content) > 2:
            if state == States.HEADER:
                state = States.BODY
            add_newline_to_section(section)
            section.Body += content

        elif content.name == 'p' and content.strong:
            contents = content.strong.contents[0]
            # Hack to remove activities
            if len(contents) > 2 and contents != "Activities":
                if state == States.HEADER:
                    section.Title += '\n' + contents
                else:
                    sections.append(section)
                    section = Section()
                    section.Title = contents
                    state = States.HEADER

        elif content.name == 'p' and content.iframe:
            pass

        elif content.name == 'p':
            contents = content.contents[0]

            if len(contents) > 2:
                if state == States.HEADER:
                    state = States.BODY
                add_newline_to_section(section)
                section.Body += contents

    sections.append(section)
    return sections


def get_conversation(url):
    html_data = get_html_text(url)
    soup = BeautifulSoup(html_data)
    body = soup.findAll('div', {"class": "body"})[0]

    for t in ["span", "ul", "li", "ol", "p > strong"]:
        for match in body.findAll(t):
            match.replaceWithChildren()

    conversation = Conversation()
    conversation.Title = get_title(soup)
    conversation.Sections = get_sections(body)
    return conversation


def to_json(obj):
    return json.dumps(obj, default=dumper, indent=2)


def get_data(urls, topic_titles):
    topics = []
    nextId = 0
    for y in range(number_of_topics):
        topic_title = topic_titles[y]
        print (topic_title)
        topic = Topic()
        topic.Title = topic_title.split(" (")[0]
        topic.Conversations = []
        for x in range(number_of_conversations_in_topic):
            conversation = get_conversation(urls[nextId])
            conversation.Id = nextId
            print(nextId)
            nextId += 1
            topic.Conversations.append(conversation)

        topics.append(topic)

    return topics


def main():
    urls = get_urls()
    topic_titles = get_topic_titles()

    if (len(urls) != number_of_conversations
            or len(topic_titles) != number_of_topics):
        print ("WARNING: we probably have a bad number" +
               " of topics or conversations!")

    topics = get_data(urls, topic_titles)
    json = to_json(topics)

    with open("conversations.json", "w") as f:
        f.write(json)

    print(json)


if __name__ == "__main__":
    main()
